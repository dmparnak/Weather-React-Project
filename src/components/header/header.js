import React from "react";
import { Navbar, Button } from "react-bootstrap";
import "./header.css";

const Header = ({logOut}) => {
    return (
        <Navbar bg="light" expand="lg" className="header">
            <Navbar.Brand className="city">Kyiv, Ukraine</Navbar.Brand>
            <Button variant="primary" onClick={logOut}>Log out</Button>
        </Navbar>
    );
}

export default Header;