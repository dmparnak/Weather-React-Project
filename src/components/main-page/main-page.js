import React, { useState } from "react";
import { Navigate } from "react-router";
import WeatherComponent from "../weather-component/weather-component";
import "./main-page.css";
import Header from "../header/header";
import LoadingSpinner from "../loading-spinner/spinner";


const getWeather = async (setWeather, setIsLoading) => {
    const url = `https://api.open-meteo.com/v1/forecast?latitude=50.45&longitude=30.52&hourly=temperature_2m&daily=temperature_2m_max,temperature_2m_min,sunrise,sunset,windspeed_10m_max&timezone=Europe%2FBerlin&past_days=5`;
    let weather = [];
    setIsLoading(true);
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            weather = data.daily;
            setWeather(weather);
            setIsLoading(false);
        })
        .catch((err) => {
            console.log(err);
            setIsLoading(false);
         });
}


const MainPage = ({ isLoggedIn, logOut }) => {
    const [weather, setWeather] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    if (!isLoggedIn) {
        return <Navigate to="/login" />
    }

    if (weather === null) {
        if (isLoading) {
            return <LoadingSpinner />
        } else {
            getWeather(setWeather, setIsLoading);
        }
    }

    const weatherElements = () => {
        const weatherElements = [];
        for (let i = 0; i < 5; i++) {
            if (weather == null) {
                weatherElements.push(<WeatherComponent key={i} data={null} number={i} />)
            } else {
                weatherElements.push(<WeatherComponent key={i} data={weather} number={i} />)
            }
        }
        return weatherElements;
    }


    return (
        <React.Fragment>
            <Header logOut={logOut} />
            <div className="weather-group">{weatherElements()}</div>
        </React.Fragment>
    );
}

export default MainPage;