import React from "react";
import { Image } from "react-bootstrap";
import "./not-found-page.css";

const NotFound = () => {
    return (
        <Image className="page image" src={require('../../images/not-found.png')}/>
    );
}

export default NotFound;