import React from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Image from "react-bootstrap/Image";
import { useNavigate } from "react-router-dom";
import "./sign-in.css";


const SignIn = ({ loggedIn }) => {
    const navigate = useNavigate();
    const [areCredentialsCorrect, setForCorrectCredentials] = React.useState(null);

    const onSubmit = (e) => {
        e.preventDefault();
        if(e.target.login.value === 'dmytro3' && e.target.password.value === 'dmytro3') {
            loggedIn();
            navigate('/weather');
            setForCorrectCredentials(true);
        } else {
            setForCorrectCredentials(false);
        }
    }

    const incorrect = areCredentialsCorrect === false ? 
    (<Form.Label className="warning">Wrong login or password</Form.Label>) : null;

    return (
        <React.Fragment>
            <Image className="image" src={require('../../images/background.jpg')}/>
            <div className="login-form">
                <p className="header-form">SIGN IN</p>
                <Form onSubmit={onSubmit}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label style={{marginRight: "20px"}}>Login:</Form.Label>
                        <Form.Control type="text" name="login" placeholder="Enter login" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label style={{marginRight: "20px"}}>Password:</Form.Label>
                        <Form.Control type="password" name="password" placeholder="Enter password" />
                    </Form.Group>
                    {incorrect}
                    <Button variant="primary" type="submit" className="submitButton">
                        Login
                    </Button>
                </Form>
            </div>
        </React.Fragment>
    );
}

export default SignIn;
