import React from "react";
import { Card } from "react-bootstrap";
import { Spinner } from "react-bootstrap";
import dayjs from "dayjs";

import './weather-component.css';
const WeatherData = ({number, data}) => {
    const date = dayjs(data.time[number]).format('DD.MM.YYYY');
    const minT = Math.round(data.temperature_2m_min[number]);
    const maxT = Math.round(data.temperature_2m_max[number]);
    const sunrise = dayjs(data.sunrise[number]).format('HH:mm:ss');
    const sunset = dayjs(data.sunset[number]).format('HH:mm:ss');
    const maxWindSpeed = Math.round(data.windspeed_10m_max[number]);

    return (
        <React.Fragment>
            <p className="date">{date.toString()}</p>
            <Card className="weather-info">
                <p>
                    <strong>Max: </strong>{maxT}°C
                </p>
                <p>
                    <strong>Min: </strong>{minT}°C
                </p>
                <p>
                    <strong>Wind speed (max): </strong>{maxWindSpeed} km/h
                </p>
                <p>
                    <strong>Sunrise: </strong>{sunrise.toString()}
                </p>
                <p>
                    <strong>Sunset: </strong>{sunset.toString()}
                </p>
            </Card>
        </React.Fragment>
    );
}

const WeatherComponent = ({ data, number }) => {
    const weatherInfo = data !== null ? (
        <WeatherData number={number} data={data} />
    ) : null;
    return (
        <div className="weather-card">
            {weatherInfo}
        </div>
    )
}

export default WeatherComponent;