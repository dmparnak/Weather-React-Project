import React from 'react';
import SignIn from './components/login/sign-in';
import MainPage from './components/main-page/main-page';
import NotFound from './components/not-found-page/not-found-page';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';

const App = () => {
  const [isLoggenIn, setLoggedIn] = React.useState(false);

  const loggedIn = () => {
    setLoggedIn(true);
  }

  const loggedOut = () => {
    setLoggedIn(false);
  }

  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" exact element={<Navigate to="/login" />}/>
          <Route path="/login" exact element={<SignIn loggedIn={ loggedIn } />} />
          <Route path="/weather" exact element={<MainPage isLoggedIn={ isLoggenIn } logOut={ loggedOut }/>} />
          <Route path="/*" element={<NotFound/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
